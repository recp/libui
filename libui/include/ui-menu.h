/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef __libui__menu__
#define __libui__menu__

#include "ui-base.h"
#include "ui-color.h"
#include "ui-geometry.h"

namespace ui {
  
class _libui_export Menu {
public:
   
};

} // namespace ui

#endif /* defined(__libui__menu__) */
