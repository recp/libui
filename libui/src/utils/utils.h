/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef __libui_win_window__
#define __libui_win_window__

#include "../config.h"

namespace ui {

} // namespace ui

#endif /* defined(__libui_win_window__) */