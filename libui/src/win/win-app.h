/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef __libui_win_app__
#define __libui_win_app__

namespace ui {

class AppImpl {
public:
  AppImpl();
  void run();
 //  void run(Window * rootWindow);

private:

};

} // namespace ui

#endif /* defined(__libui_win_app__) */